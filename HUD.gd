extends CanvasLayer

signal start_game

func show_message(text) -> void:
	$Message.text = text
	$Message.show()
	$MessageTimer.start()
	
func show_game_over() -> void:
	show_message("YOU DIED")
	yield($MessageTimer, "timeout")
	
	$Message.text = "DODGE THE CREEPS"
	$Message.show()
	yield(get_tree().create_timer(1.0), "timeout")
	$StartButton.show()
	
func update_score(score) -> void:
	$Score.text = convert(score, TYPE_STRING)

func _ready() -> void:
	pass


func _on_MessageTimer_timeout() -> void:
	$Message.hide()


func _on_StartButton_pressed() -> void:
	$StartButton.hide()
	emit_signal("start_game")
