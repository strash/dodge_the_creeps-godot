extends Node

export (PackedScene) var Mob

var score: int = 0

func new_game() -> void:
	$Player.start($StartPosition.position)
	$Player.show()
	score = 0
	$StartTimer.start()
	$BackgroundMusic.play()
	$HUD.update_score(score)
	$HUD.show_message("GET READY")

func _on_Player_hit() -> void:
	$ScoreTimer.stop()
	$MobTimer.stop()
	$HUD.show_game_over()
	get_tree().call_group("mobs", "queue_free")
	$BackgroundMusic.stop()
	$DeathSound.play()

func _on_StartTimer_timeout() -> void:
	$MobTimer.start()
	$ScoreTimer.start()
	$HUD.update_score(score)

func _on_MobTimer_timeout() -> void:
	$MobPath/MobSpawnLocation.offset = randi()
	var mob = Mob.instance()
	add_child(mob)
	var direction = $MobPath/MobSpawnLocation.rotation + PI / 2
	mob.position = $MobPath/MobSpawnLocation.position
	direction += rand_range(-PI / 4, PI / 4)
	mob.rotation = direction
	mob.linear_velocity = Vector2(rand_range(mob.min_speed, mob.max_speed), 0)
	mob.linear_velocity = mob.linear_velocity.rotated(direction)

func _on_ScoreTimer_timeout() -> void:
	score += 1
	$HUD.update_score(score)

func _ready() -> void:
	randomize()
	$Player.hide()
