extends RigidBody2D

export var min_speed: int = 150
export var max_speed: int = 300

func _ready() -> void:
	var mobs = $AnimatedSprite.frames.get_animation_names()
	$AnimatedSprite.animation = mobs[randi() % mobs.size()]


func _on_VisibilityNotifier2D_screen_exited() -> void:
	queue_free()
